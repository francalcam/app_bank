#load 'models/transfer.rb'
#load 'models/user.rb'
load 'models/seed.rb'

RSpec.describe "Transfer check", :type => :request do

  describe "Correct send" do

    it "With commision ('inter-bank')" do
      Seed.new
      #sleep 1
      current_user = User.all.find{ |user| user.login == "jim" }
      destination_user = User.all.find{ |user| user.login == "emma" }
      origin_account = Account.all.find{ |account| account.user_id == current_user.id }
      destination_account = Account.all.find{ |account| account.user_id == destination_user.id } unless destination_user.nil?
      transfer = Transfer.new(amount:1000, origin_account: origin_account, destination_account:destination_user.login)
      transfer.send_money(status:true, test:true)
      expect(origin_account.money).to eq(35195)
      expect(destination_account.money).to eq(20850)
    end
  end

  describe "Correct send" do
    it "Without commision ('intra-bank')" do
      seed = Seed.new
      #sleep 1
      current_user = User.all.find{ |user| user.login == "jim" }
      destination_user = User.all.find{ |user| user.login == "jim" }
      origin_account = Account.all.find{ |account| account.user_id == current_user.id }
      destination_account = Account.all.find{ |account| account.user_id == destination_user.id } unless destination_user.nil?
      resource = Transfer.new(amount:1000,origin_account: origin_account, destination_account:destination_user.login)
      resource.send_money(status: true, test: true)
      expect(origin_account.money).to eq(36200)
    end
  end

  describe "Error send" do
    it "With status false" do
      seed = Seed.new
      #sleep 1
      current_user = User.all.find{ |user| user.login == "jim" }
      destination_user = User.all.find{ |user| user.login == "emma" }
      origin_account = Account.all.find{ |account| account.user_id == current_user.id }
      destination_account = Account.all.find{ |account| account.user_id == destination_user.id } unless destination_user.nil?
      transfer = Transfer.new(amount:1000, origin_account: origin_account, destination_account:destination_user.login)
      transfer.send_money(status: false, test:true)
      expect(origin_account.money).to eq(36200)
      expect(destination_account.money).to eq(19850)
    end
  end

end
