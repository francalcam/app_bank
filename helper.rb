class String
  def is_int?
    self.to_i.to_s == self
  end
end

def input(prompt)
  puts(prompt)
  value = gets.chomp
  cls
  value
end

def puts_cls(text)
  puts(text)
  sleep 2
  cls
end

def cls
  system "clear"
end

def current_user
  Session.all.first.user
end

def select_option(options)
  option_key = nil
  loop do
    puts "Please, select an option:"
    options.each_with_index do |option,index|
      text = option[1].first
      puts "#{index + 1}. #{text}"
    end
    option = gets.chomp
    cls
    if (1..options.count).map(&:to_s).include?(option)
      option_key = options.to_a[option.to_i - 1].first
      break
    else
      puts "Wrong choice! Please select a valid numeric option"
    end
  end
  option_key
end

def transfer_form
  @origin_account = Account.all.find{ |account| account.user_id == current_user.id }
  # Destinatiion
  loop do
    number_or_user = input "Please, enter the account number or login to whom you want to transfer the money:"
    @destionation_account = Account.all.find{ |account| account.number == number_or_user }
    if @destionation_account.nil?
      user = User.all.find{ |user| user.login == number_or_user }
      @destionation_account = Account.all.find{ |account| account.user_id == user.id } unless user.nil?
    end
    if @destionation_account
      break
    else
      puts_cls "There is no user or account with this data"
    end
  end
  # Typology
  if @origin_account && @destionation_account
    @typology = @origin_account.bank_id == @destionation_account.bank_id ? "intra-bank" : "inter-bank"
  end
  # Amount
  loop do
    @amount = input "Please, enter the amount:"
    if @amount.is_int?
      @amount = @amount.to_i
      if @typology.eql?("inter-bank") && @amount > 1000
        puts_cls "Sorry, inter-bank operations have a limit of 1000€"
      elsif @amount > @origin_account.available_money
        puts_cls "Sorry, the balance is insufficient"
      else
        break
      end
      break
    else
      puts_cls "Please, enter a numerical value"
    end
  end
end
