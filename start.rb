#!/usr/bin/env ruby

require_relative 'models/seed'
require_relative 'models/bank'
require_relative 'models/user'
require_relative 'models/account'
require_relative 'models/transfer'
require_relative 'models/session'

require_relative 'helper'

seed = Seed.new
option=nil
loop do
  session = Session.new
  if session.user
    loop do
      option = select_option(seed.options)
      if option.eql?(:exit) || option.eql?(:logout)
        puts_cls "Thanks, see you soon #{session.user.name}"
        session=nil
        break
      end
      # Get object
      resource = Object.const_get(option.capitalize)
      # Initialize/Index
      method_name = seed.options[option].last
      obj = resource.send(method_name) rescue nil
      obj.send_money if obj && option==:transfer
    end
  end
  break if option.eql?(:exit)
end
