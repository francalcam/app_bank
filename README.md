• Instructions:
  App:
  To run the application, just launch the following command from a terminal:
  ./start.rb
  I have implemented a "session" start. To log in, the password users are:
  User - Password - Bank
  jim    1234       Evo
  fran   1234       Evo
  emma   1234       Caixa Bank
  iñigo  1234       Caixa Bank

  Select the menu options by entering the number and pressing enter.
  For transfers, they can be made with the account number or with the username of the user you want to send it to.
  You can change the session to verify that the data of the user accounts is correct (menu option 3)

• Tests:
  To launch the tests from the application directory you must execute:
  rspec spec/features/simple_test_spec.rb

• Questions:
  - How would you improve your solution?
  1.0 I would implement a transfer typology model so that commissions or any other proprietary of its own could be applied there. So we could modify it quickly and easily.
  2.0 I would lock the money available until the transaction was finalized.
  3.0 All the logic would be put into a database transaction in case a rollback fails to be launched

- How would you adapt your solution if transfers are not instantaneous?
  I would use a queuing system to launch the tasks in the background avoiding that it penalizes performance (sidekiq, delayed job)
