class Bank
  attr_accessor :id, :name

  def initialize(params)
    @id=params[:id]
    @name=params[:name]
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end
end
