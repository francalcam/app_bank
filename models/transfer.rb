require_relative '../helper'

require_relative "user"
require_relative "account"

class Transfer
  attr_accessor :typology_id, :account_id, :destination_account, :amount, :status, :typology, :has_commision, :created_at

  def initialize(params={})
    @origin_account = params[:origin_account] || get_origin
    @destination_account ||= get_destination(params[:destination_account])
    @typology = params[:typology] || get_typology
    @amount = params[:amount] || get_amount
  end

  def get_origin
    Account.all.find{ |account| account.user_id == current_user.id }
  end

  def get_destination(destination = nil)
    destination_account = nil
    loop do
      number_or_user = destination || input("Please, enter the account number or login to whom you want to transfer the money:")
      destination_account = Account.all.find{ |account| account.number == number_or_user }
      if destination_account.nil?
        user = User.all.find{ |user| user.login == number_or_user }
        destination_account = Account.all.find{ |account| account.user_id == user.id } unless user.nil?
      end
      if destination_account
        break
      else
        puts_cls "There is no user or account with this data"
      end
    end
    destination_account
  end

  def get_typology
    typology = nil
    if @origin_account && @destination_account
      typology = @origin_account.bank_id == @destination_account.bank_id ? "intra-bank" : "inter-bank"
    end
    typology
  end

  def get_amount
    amount = nil
    loop do
      amount = input "Please, enter the amount:"
      if amount.is_int?
        amount = amount.to_i
        if @typology.eql?("inter-bank") && amount > 1000
          puts_cls "Sorry, inter-bank operations have a limit of 1000€"
        elsif amount > @origin_account.available_money
          puts_cls "Sorry, the balance is insufficient"
        else
          break
        end
        break
      else
        puts_cls "Please, enter a numerical value"
      end
    end
    amount
  end

  def send_money(params={})
    @status = params[:status]
    @status = is_ok? if @status.nil?
    @created_at = Time.now.strftime("%d/%m/%Y %H:%M")
    if @typology.eql?("inter-bank") && @amount > 1000
      puts_cls "Sorry, inter-bank operations have a limit of 1000€"
      @status = false
    elsif @amount > @origin_account.available_money
      puts_cls "Sorry, the balance is insufficient"
      @status = false
    elsif @status
      @has_commision = has_commision?
      @origin_account.available_money -= @amount
      @origin_account.money -= @amount
      @origin_account.money -= 5 if @has_commision
      @origin_account.available_money = @origin_account.money
      @destination_account.available_money += @amount
      @destination_account.money += @amount
      puts_cls "Congratulations, successful transfer" unless params[:test]
    else
      @origin_account.available_money += @amount
      puts_cls "Sorry, has there been any problem!" unless params[:test]
    end

  end

  def is_ok?
    percentage = rand(1..100)
    #puts percentage
    percentage >= 30
  end

  def typology
    if @origin_account && @destination_account
      @origin_account.bank_id == @destination_account.bank_id ? "intra-bank" : "inter-bank"
    end
  end

  def has_commision?
    typology.eql?("inter-bank")
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end

end
