require_relative '../helper'

class Account
  attr_accessor :id, :money, :bank_id, :available_money, :number, :user_id

  def initialize(params)
    @money = params[:money]
    @available_money = params[:money]
    @number = params[:number]
    @user_id = params[:user_id]
    @bank_id = params[:bank_id]
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end

  def self.index
    cls
    your_accounts = all.select{ |account| account.user_id == current_user.id }
    your_accounts.each do |account|
      puts "Account #{account.number}:"
      puts "Money: #{account.money}"
      puts "Avalible money: #{account.available_money}"
      puts ""
      transfers = Transfer.all
      sep = "\t"
      if transfers.any?
        puts "Banking transactions:"
        puts "Destionation acount#{sep}Amount#{sep}Typology#{sep}Status#{sep}Date"
        transfers.select{ |trans| trans.account_id == account.id }.each do |trans|
          puts "#{trans.destination_account.number}#{sep}#{trans.amount}#{sep}#{trans.typology}#{sep}#{trans.status}#{sep}#{trans.created_at}"
        end
      end
      puts ""
      input "*Press enter to return to the start menu"
    end
  end


end
