#require_relative "interface"
require_relative '../helper'

class Session

  attr_accessor :user

  def initialize
    wellcome
    loop do
      @user = get_user(User.all)
      continue = input "Want to try again? (y/n)" if @user.nil?
      break unless @user.nil? && ["yes","y","YES","Y"].include?(continue)
    end
    puts @user.nil? ? "See you soon." : "Wellcome #{@user.treatment}. #{@user.name} #{@user.surname}!"
    sleep 2
    cls
  end

  def wellcome
    cls
    message  = "# Welcome to your app bank!!! #"
    ornament = "#"*message.length
    puts ornament
    puts message
    puts ornament
    sleep 2
    cls
  end

  def get_user(users)
    login = input("Please enter your e-mail or login.")
    pass  = input("Please enter password.")
    #raise users.inspect
    user  = users.find { |u| (u.login.eql?(login) || u.email.eql?(login)) && u.password.eql?(pass)}
    if user.nil?
      puts "User or password incorrect!"
      sleep 2
    end
    cls
    user
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end

end
