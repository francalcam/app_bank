require_relative "bank"
require_relative "user"
require_relative "account"
require_relative "transfer"
require_relative "session"

class Seed

  attr_accessor :options

  def initialize
    b1 = Bank.new(id:1, name:"Evo Bank")
    b2 = Bank.new(id:2, name:"Caixa Bank")
    u1 = User.new(id:1, name:"Emma", surname:"Garcia", treatment:"Ms", email:"emma@rsi.com", login: 'emma', password: "1234")
    u2 = User.new(id:2, name:"Jim", surname:"Perez", treatment:"Mr" , email:"jim@rsi.com", login: 'jim', password: "1234")
    u3 = User.new(id:3, name:"Iñigo", surname:"Ranedo", treatment:"Mr" , email:"iñigo@rsi.com", login: 'iñigo', password: "1234")
    u4 = User.new(id:4, name:"Fran", surname:"Calahorro", treatment:"Mr" , email:"fran@rsi.com", login: 'fran', password: "1234")
    a1 = Account.new(id: 1, user_id: u1.id, bank_id: b1.id, money: 19850, number: "4716-7268-7199-0138")
    a2 = Account.new(id: 2, user_id: u2.id, bank_id: b2.id, money: 36200, number: "5475-8703-9090-9324")
    a3 = Account.new(id: 3, user_id: u3.id, bank_id: b1.id, money: 45200, number: "5174-4127-2293-1157")
    a4 = Account.new(id: 4, user_id: u4.id, bank_id: b2.id, money: 20150, number: "2599-7879-5643-8829")
    @options = {account: ["Accounts", :index], transfer: ["New transfer", :new], logout:["Logout", :Logout], exit:["Exit", :exit]}
  end

end
