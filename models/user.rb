class User

  attr_accessor :id, :name, :surname, :login, :treatment, :email, :password

  def initialize params={}
    @id = params[:id]
    @name = params[:name]
    @login = params[:login]
    @email = params[:email]
    @surname = params[:surname]
    @password = params[:password]
    @treatment = params[:treatment]
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end

end
